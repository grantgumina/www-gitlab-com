---
layout: job_family_page
title: "Support Operations"
---

## Support Operations Specialist

As a Support Operations Specialist, you will be responsible for supporting the day-to-day
operations and software systems used by GitLab’s global Technical Support team, primarily
our Zendesk instance, and integrations with internal business processes and tools.
You will be able to juggle a diverse workload that includes everything from managing
user provisioning to troubleshooting custom applications in support of a global organization.
As we rely on our systems to support and scale the organization, you will be the backbone and the foundation to our success!

## Responsibilities

- Develop a flexible process and tool execution strategy to support the growth and scalability objectives of GitLab Support and company wide initiatives.
- Own and drive projects to completion in agreement with relevant stakeholders in a cross-departmental function.
- Manage a fast-paced queue of support operations requests, prioritize requests according to business impact, and drive to appropriate completion dates.
- Support and maintain several business critical SaaS systems (e.g. Zendesk Suite, GitLab, Slack, SFDC, Google groups, Zendesk Insights/Explore, and GitLab internal systems) including user administration in accordance with written and audited security controls and configuration changes when needed.
- Develop and maintain system failover processes for customer facing Zendesk apps.
- Provide support and troubleshooting for our Support systems, including App integration, tagging, macros, automations, etc. when necessary.
- Maintain and demonstrate 100% compliance with all written security policies and change management controls and assist with quarterly audits of user access to key systems.
- Understand internal customer's needs and business context to provide outstanding support and maintain high customer satisfaction.
- Assist with implementation of new systems as needed to support evolving processes and critically evaluate our use of systems with a view to improve global efficiency.

## Required Skills/Experience:

- 1-3 years experience in SaaS support with proven ability to support diverse customers needs with skill and humor
- Zendesk Administrator or equivalent enterprise application system administration experience preferred with the ability to gain Zendesk Administrator Certification within 6 months of start date required.
- Demonstrated technical aptitude for, and experience supporting, client-server and/or web-based applications and SaaS/PaaS solutions
- Proven ability to solve practical business problems
- Understanding of business processes and ability to translate business requirements into application functionality
- Zendesk enthusiast who thrives on working in a fast-paced and changing environment
- Strong team player with service-oriented attitude and customer focus
- Excellent written and verbal communication
- BA/BS Degree or equivalent work experience
- An eye for detail and out-of-the-box thinking
- You share our values, and work in accordance with those values
- Successful completion of a background check
- Ability to use GitLab

## Desired Skills:

- Demonstrated understanding of technical software support processes and concepts
- Experience in CRM or a related industry
- Familiarity with change management processes and risk control compliance
- Experience working on the Zendesk platform as an agent or developer
- Experience with enterprise integration tools 

## Performance Indicators

Support Operations support the global Support team to achieve the following performance indicators by ensuring we have the most efficient workflows through our applications;

* [Average ticket resolution across the team](/handbook/support/#average-monthly-tickets-per-support-team-member)
* [Service Level Agreement](/handbook/support/#service-level-agreement-sla)
* [Ticket deflection through docs search](/handbook/support/#ticket-deflection-through-documentation)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Selected candidates receive a short assessment from our Global Recruiters
* Candidates will then be invited to schedule a 45 minute behavioral interview with a Support Engineering Manager
* Candidates will then be invited to schedule a 45 minute behavioral interview with our Director of Support
* Candidates may be invited to schedule an additional 45 minute interview with the VP of Engineering
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Support Operations Manager

As a Support Operations Manager, you are responsible for being a technical expert in Zendesk system administration along with data and business process analysis. Additionally, you will need to be proficient across multiple other cloud-based application services.  We are building world-class and industry-leading best practices to ensure our Support Engineers and Support Agents deliver unparalleled customer experiences.
We are seeking a highly motivated Support Operations Manager who is organized, cool under pressure, a leader, and an innovative problem solver; someone with fantastic troubleshooting and analytical skills, well-rounded experience working in customer support or other operational roles, and is motivated by team as well as personal success.  Your work mantra is to solve the problem, not the symptom.


## Responsibilities

- Hire, develop, and manage a team of specialists to complete projects focused on excellence in the operations of a global Support Engineering organization.
- Develop processes and execution strategy toward a Support Operations Roadmap to support the growth and scalability objectives of GitLab Support and contribute to a rolling 6-month plan in order to drive towards department and company wide initiatives.
- Own and drive projects to completion in agreement with relevant stakeholders in a cross-departmental function.
- Contribute to the administering all software systems used by GitLab’s Technical Support team (e.g. Zendesk Suite, GitLab, Slack, SFDC, Google groups, Insights/Explore, and GitLab internal systems) including user administration in accordance with written and audited security controls and configuration changes when needed.
- Develop processes to prioritize and implement configuration changes and feature deployments toward a common vision.
- Partner with coworkers, managers, and executives, as well as external partners to gather system requirements, understand business needs, and identify resources as necessary to ensure customer and employee success.
- Work with GitLab Support leadership, define workflows, practices, and standards to ensure that Support Engineers and Support Agents can deliver fast, reliable, and accurate customer support globally.
- Work with management to develop Key Performance Indicators (KPIs) for the systems and tools administered by the team; capture and report those KPIs regularly to confirm system health.  Improve process and education around analysis and resolution of escalated customer and/or technical issues in a timely and effective manner.
- Drive towards becoming a Subject Matter Expert (SME) in product or process areas as needed.


## Requirements:

- 3+ Years Customer Support or Operations experience required, preferably in a global organization.
- Zendesk Administrator or equivalent enterprise application system administration experience preferred with the ability to gain Zendesk Administrator Certification within 6 months of start date required.
- 3+ years experience managing and leading a team of people, including goal-setting and leading personal development.
- Content management experience; writing and troubleshooting JSON and/or JavaScript and HTML; familiarity with principles of change management.
- Experience implementing business processes, sufficient for regulatory and security compliance and working with auditors.
- Ability to use GitLab
- You share our [values](https://about.gitlab.com/handbook/values) and work in accordance with those values.

## Desired Skills:

- Demonstrated understanding of technical software support processes and concepts
- Experience in CRM or a related industry
- Familiarity with change management processes and risk control compliance
- Experience working on the Zendesk platform as an admin or developer
- Experience with enterprise integration tools
- Demonstrated leadership, analytical, communication, and problem-solving skills and the ability to act/decide accordingly.  
- Ability to collect, synthesize, and research complex issues and diverse information.
- Exceptional customer service skills and the ability to plan organize and exercise sound judgment.
 

## Performance Indicators

Support Operations support the global Support team to achieve the following performance indicators by ensuring we have the most efficient workflows through our applications;

* [Average ticket resolution across the team](/handbook/support/#average-monthly-tickets-per-support-team-member)
* [Service Level Agreement](/handbook/support/#service-level-agreement-sla)
* [Ticket deflection through docs search](/handbook/support/#ticket-deflection-through-documentation)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Selected candidates receive a short assessment from our Global Recruiters
* Candidates will then be invited to schedule a 45 minute behavioral interview with a Support Engineering Manager
* Candidates may then be invited to a [panel interview](https://about.gitlab.com/handbook/hiring/interviewing/panel/) with 2 to 3 Support Engineering Managers.
* Candidates will then be invited to schedule a 45 minute behavioral interview with our Director of Support
* Candidates may be invited to schedule an additional 45 minute interview with the VP of Engineering
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
