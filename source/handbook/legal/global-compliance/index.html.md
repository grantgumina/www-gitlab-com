---
layout: handbook-page-toc
title: "Compliance for Your Average Human"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Compliance Topics Demystified 

## Principle of Compliance

I had lunch with my friend Mel the other day.  She is such a healthy person that I thought it would be great to emulate such a pristine lifestyle. 

The waiter took our orders.  

Mel ordered a nice spinach salad.  Protein?  Yes, grilled salmon; complemented with cucumber, radishes, scallions, sprinkled with croutons with a cruet of oil and vinaigrette.  A French baguette on the side.  Mel was ready to embark on her healthy meal.

Me?  I ordered the same thing.  Except…I substituted the grilled salmon with fried chicken.  I replaced the croutons with French Fries and the baguette with biscuits. Oh, yeah, one more substitution - replace the oil and vinaigrette with gravy. Hold the cucumbers, radishes, and scallions.  I can do a spinach salad if modified correctly.

Mel seemed to like her salad.  Mine was okay...except for the spinach.

Sometimes we can find so many loopholes in the rules that we actually alter the underlying principle of the matter…and principles matter.  Everything we do matters. At GitLab, we want you to comply with the policies and procedures because it is the right thing to do; but most importantly, we want you to comply because we value integrity.

Paul Harvey once told an amazing story about a football ploy utilized by Carlisle University in the early 1900’s.  Carlisle was getting ready to play Syracuse.  Carlisle’s Coach Warner was determined to defeat Syracuse so he poured over the rule book to find any loophole he could exploit.  He found one.  Coach Warner had realistic footballs sewn onto the front of every player’s jersey on his team.  The football appliqués were puffed out so they actually appeared 3-dimensional.  The result is that when the game was in play, Syracuse couldn’t tell which Carlisle player actually had the ball.  The deceptive jerseys made it difficult to determine who to tackle.  The result – Carlisle won.

The next game was to be a heated one as it was between Carlisle and Harvard.  The two colleges were rivals and the stakes were high.  Despite the strong rivalry, the coaches respected one another. Harvard’s coach met with Coach Warner and inquired if he planned to have Carlisle don the deceptive uniforms in the upcoming game.  Coach Warner indicated that there were no rules that prevented him from doing so; so, yes, yes his team would be wearing the jerseys.  

Game day came.  Harvard was the home team.  Carlisle was the guest.  Carlisle took the field first in their appliquéd football garb.  Harvard then took the field in their **crimson** colored uniforms. As the home team, Harvard had the duty to bring the game balls.  The refs took the field and grabbed the bag of balls provided by the home team.  They reached in and discovered a bag full of **crimson** colored footballs.  

Harvard won. 

The point of the story is that people who use the rules as the baseline or absolute minimum for integrity are apt to look for loopholes.  Those who follow principles, do not.   Doing the right thing because it is the right thing ultimately wins the long game. 

## GDPR

**GDPR is here…**
The General Data Protection Regulations came into effect in the European Union May, 2018.  Anyone who handles personal information of Europeans must comply.  In the European Union, privacy of personal information is a right – like human dignity, freedom, equality and the opposite of left.  It is a personal right belonging to the owner of the information and cannot be commoditized, utilized, processed without that person’s consent.  The European Union (EU) and European Economic Area (EEA) diligently try to preserve the rights of their citizens but such efforts were without coordinated and express direction – until now.  The GDPR reflects a unified approach to handling personal, sensitive information in the EU/EEA. 

**What is covered by the GDPR?**
The regulation is implemented in all local privacy laws across the entire EU and EEA region. It applies to all companies selling to and storing personal information about citizens in Europe, including companies on other continents (so even offices in Antarctica must comply if they intend to access personal information of Europeans.)  GDPR provides citizens of the EU and EEA with greater control over their personal data and assurances that their information is securely protected. According to the GDPR directive, personal data is any information related to a person such as a name, a photo, an email address, bank details, updates on social networking websites, location details, medical information, financial information or a computer IP address. Ultimately, data that can directly or indirectly identify a person is covered by GDPR.

**What do these regulations involve?** 
There are eight fundamental principles under the GDPR.  Let’s bring it down to a personal application to help navigate. Imagine you left your electronic diary on your Ex’s server.  You remember that diary; the one with all those deep, dark secrets like the time you went into a dressing room and then yelled real loudly, “There’s no toilet paper in here!”?  Those crazy times from your past can haunt you – unless you are European.  Well, they can still haunt you - but not electronically.  You want your diary back, you need it back and you need to know how much damage was done. Congratulations! In our hypothetical, you are European!  (You may be European outside of the hypothetical, as well, but stay with me here.) 

**Applying the GDPR to your diary, what are your rights?**

**The right to access** – Individuals have the right to request access to their personal data and to ask how their data is used by the company after it has been gathered. The company must provide a copy of the personal data, free of charge and in electronic
format if requested. 

* So, your diary is on your Ex's server.  You have the right to see exactly what dates and entries are stored.  And, of equal importance, you want to know, specifically, with whom your Ex shared that information and why. (Details!  You want details!) You would not want to hear a generic answer like “The contents of the diary were shared with the Finance department, the customers at the local pub and numerous bathroom stalls… for everyone’s reading enjoyment.”  You want specificity about what was shared and with whom. And there had better have been a good reason for any sharing that occurred. 

**The right to be forgotten** – Individuals can withdraw their consent from a company to use their personal data; they have the right to have their data erased.

*  You can demand your diary be shredded, burned, flambéed, sautéed with a nice Hollandaise sauce or simply destroyed (for the more likely and less dramatic).  In short, all evidence of the contents of your book must be erased.  There cannot be excerpts of the diary floating about on social media, bathroom walls or billboards.  The existence, use and memory of the information must disappear faster than cash in my wallet.  

**The right to data portability** – Individuals have a right to transfer their data from one service provider to another. And it must happen in a commonly used and machine readable format.

*  Maybe you separated on good terms and you think your Ex’s friend is pretty cute.  You can ask to have your diary delivered to that humorous, single friend in an unaltered, readable format.  Generally, this is a more common request with medical records, academic transcripts and the like but those aren't the subject of this hypothetical.

**The right to be informed** – this covers any gathering of data by companies.  Individuals must be informed before data is gathered. Individuals must opt in for their data to be gathered, and consent must be freely given rather than implied. 

*  If your Ex decides to go find evidence of your past based on your contents, you have to give permission for each planned encounter.  You can allow your Ex to visit your third grade teacher, obtain your 9th grade report card and visit your first childhood address.  Nothing more.  If your Ex decides to get details about your juggling statistics, your Ex crossed the line.  Just because your Ex has your diary- doesn’t mean you intend for the contents to be read, evaluated, processed, extrapolated or shared.  Consent must be specific, clear and express. There can’t be the broad, sweeping comment from your Ex that “I will only share your information on days that start with ‘T’… Today and Tomorrow; and only with people with a pulse.”  There must be clarity in how and with whom the information is shared.

**The right to have information corrected** – Individuals can have their data updated if it is out of date, incomplete or incorrect. 

*  You told your Ex that the diary excerpt from May 16th can be posted on Facebook.  Your Ex posts that you said “I have enough money to live happily the rest of my life...”  You can demand it be corrected to include the last part of the sentence which stated “...if I die on
Thursday.”      

**The right to restrict processing** – Individuals can request that their data is not used for processing. Their record can remain in place, but not be used. 

*  Your diary can stay on your Ex's server but it better not be shared, analyzed or used for anything.  If your Ex decides to compile a little black book blog and wants to invoke a ranking system with stars based on the entries in your diary – you can refuse.   

**The right to object** – this includes the right of individuals to stop the processing of their data for direct marketing. There are no exemptions to this rule, and any processing must stop as soon as the request is received. In addition, this right must be made clear to individuals at the very start of any communication.

*  If you find out your Ex is, in fact, continuing with the little black book blog – you can demand a complete stop.  Your Ex cannot object or try to find some loophole.  The diary is closed.  The conversation is closed.  The end.

**The right to be notified** – If there has been a data breach which compromises an individual’s personal data, the individual has a right to be informed within 72 hours of first having become aware of the breach. 

*  If your Ex left your diary open and unencrypted on a laptop at a cafe, your Ex better tell you quickly and with as much detail as possible of the events surrounding the loss of the sensitive information. Your Ex should expect reprisals for any careless behavior that alerted some third party to your private life.

Marc Antony said in Shakespeare's Julius Caesar, “The evil that men do lives after them.  The good is oft interred with their bones.”  Poor Caesar should’ve waited for the GDPR.  He could have better controlled the information dispersed about him.  Good thing he didn’t have a diary.

For more information, reach out to GitLab's [Data Protection Officers](/security/#data-protection-officers)

## Secrets

I’m only sharing this with you because I trust you.  I know that you know how to keep secrets. Do you realize how many secrets you are entrusted with each day?  There are three primary categories of company secrets that are super special: Trade Secrets, Confidential Information and Personally Identifiable Information (Sensitive Information if you parlez OUS).  Collectively, these things are classified as RED in our Data Classification Policy.  The “Red” designation is for the most sensitive, restricted business information that requires heightened internal controls by limiting access to only those authorized individuals with a need-to-know.  Not everyone gets to know this inside scoop.  

**Trade Secrets** are those extra bits of information that set our company apart – the chocolate chips to our cookies.  It’s that stuff that is so cutting edge that we don’t even want to get a patent on it for fear of letting others know the technology.  If you patent something, the secret ingredients are laid out for the world to see like some crazy technological recipe.  Others are precluded from copying your recipe but once they see the basic ingredients, it doesn’t take long to realize that butter can be replaced with oil;  basic flour and yeast can be replaced with self-rising flour and sugar can be replaced with gummy bears (clearly, you do not want to eat my cooking.)   Competitors quickly mobilize for comparable product offerings.  But! where there is a trade secret, competitors are left scratching their heads. Have you ever wondered what the recipe is for Coca-Cola?  Do you realize that you don’t know the recipe because the employees are very good about protecting its secrets.  There is no patent.  If there was, we would all be drinking knock off Cokes that taste like the real thing.  Once you release a trade secret – there is no protection.  It is all out there for the world to exploit; this is why we have employees sign non-disclosures and occasionally pink swear to protect the information. 

Another set of secrets include **Confidential Information**.  This is more than the general confidential stuff.  This includes the provocative business data that helps us run strategically and includes business development plans/strategies, non-public financial information, third party data and government protected information.  (Trade Secrets technically fall under this category but given its unique stature, it gets celebrity status.)  Failing to protect heightened confidential information could cause all sorts of bad feelings with GDPR, EUCI, SOX, EUEC, DOJ, DOC, SEC, OAIC and others.  In other words, if you fail to execute proper controls and restraint, you will be attacked by nothing short of alphabet soup with a badge. 

Other secrets include **Personally Identifiable Information (PII)** [parfois connu comme Sensitive Information].  This is any information that links a particular piece of sensitive information to a specific person.  For instance, if I tell you someone makes a million dollars a year.  You would probably respond with “Thank you, Captain Obvious.” That statement isn't really PII.  It's rather generic and could apply to anyone (unfortunately, not me.)  If I tell you that Jaye makes a million dollars a year - that information is interesting and even intriguing; but this, in and of itself, in not PII. There are a million Jayes in the world, it could be anyone.  Now, if I told you that Jaye Projectexample makes a million dollars year, I have now divulged PII.  You are now able to link a specific piece of sensitive information to a particular person.  This information becomes something you can exploit.  Now that you know this information - you are going to want to befriend Jaye, have her take you out to dinner, have her cosign a loan, apply for her position…  She has now been stripped of the privacy that she probably wanted to maintain. If my salary statement were true, I would be in serious trouble. 

There are various types of PII.  There is Personal Health Information (PHI) which includes sensitive information about a person’s health or medical condition; Personal Credit Information (PCI) which includes credit card information; Personally Identifiable Financial Information (PIFI) which includes banking information, account numbers, and account information; PI which is 3.14159265359; and PIE which is a more conservative dessert approach. [The secret is already out on these last two.]  How do you protect PII?  Consider the guidelines in the Data Classification Policy and apply it to all of the PII listed above (except PI whose protection is irrational and PIE whose protection won’t last five minutes unattended in my house).  

Unauthorized disclosure of Red, restricted information, could cause serious adverse impact to GitLab, its clients, business partners, and suppliers.  Your mission, should you decide to accept it…and you should accept it (not really optional), is to ensure proper protection for all data classified as Red.  

As always, feel free to reach out with questions or concerns. This message will self-destruct in five minutes.

## Personal Credit Information (PCI)

My credit card was stolen last week.  My husband isn’t reporting it though because, apparently, the thief spends less than I do.    It’s not that I’m frivolous with my spending, it’s just that there is Nigerian prince that I’ve been helping out.  I don't like to talk about it.  

Credit card information falls into the bucket of Personal Credit Information (PCI).  PCI requires heightened levels of protection - much like Personal Health Information (PHI), Personal Identifiable Information (PII), Personal Financial Information (PFI) and other Sensitive Information.  PCI is restricted information with a data classification of Red. PCI isn’t only governed by law; it is also heavily regulated by the credit card industry. Failure to properly handle credit card or bank information could result in the company not being able to process credit card orders.  Nobody wants to resort to checks – that is sooo 1980’s.  So, please, keep in mind - and apply - the Data Classification requirements for a Red level when dealing with PCI, PHI, PFI, PII, trade secrets and those other juicy bits of information we are bound to protect. 

The fundamental requirements for PCI are:
1. Must have a maintained firewall;
2. Custom passwords and unique security measures;
3. Secure/encrypt cardholder data at rest;
4. Encrypt any cardholder data transmitted;
5. Current, active and updated anti-virus must be installed;
6. Secured systems and applications must be sustained;
7. Access is on a need-to-know basis only;
8. Unique data identifiers are required for anyone who has access to cardholder data;
9. Physical access to cardholder data must be restricted;
10. Access to cardholder data needs to be logged and reported;
11. Ensure that security system and process tests are run frequently; and
12. There should be policies around all of the above.

When dealing with PCI, at no time should an entire credit card number be visible in its full form.  Generally, only the last four numbers of a card should be maintained.  Please make sure that there is no credit information ever jotted down on sticky notes, files, napkins, etc.  If you are aware of any credit card numbers that are not secure, please alert Compliance. 

As a young child, my mother told me I can be anyone I want to be...turns out this is called identity theft.  Don’t go down that road.  Let’s keep things on the up and up.

## Data Protection Impact Assessments or DPIAs

Recently, legislators threw alphabet soup at companies. In short, the EU, under the GDPR, requires DPIAs in order to be an A-OK VIP in the technology world.   In long, the European Union, under the General Data Protection Regulations, require Data Protection Impact Assessments in order to be compliant leaders in the technology world.  Translation: We need to make sure we have our finger on the pulse of any application or system integration involved with GitLab.

While the legal requirements may seem cumbersome, the need is quite apparent.

Consider the controls you would expect from your bank. Do you have locks on the doors? Do you have a safe?  Is the safe code confidential or is posted on a sticky note beside the lock? Can anyone enter the safe or is it limited to select employees?  Just what goes in the safe to begin with? Will there be free lollipops?

You expect a bank to protect your money.  A person’s identity is far more valuable; therefore, should be protected to the highest level.  Therefore any new system or data integration within GitLab must have an analysis conducted to determine if a DPIA is required.  We have combined the analysis and the DPIA to improve efficiency and ensure proper oversight of the sought after application/integration.  

Just like the various assurances with your financial institution, a DPIA ensures all the necessary controls are in place to cover your most important assets - personal information.

For more information, check out:
[DPIA Policy[(https://about.gitlab.com/handbook/engineering/security/dpia-policy/)
[DPIA Procedure](/handbook/finance/procure-to-pay/)
[DPIA Form](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/new?issuable_template=Data%20Protection%20Impact%20Assessment)

## Data Protection Impact Assessment Frequently Asked Questions DPIA FAQ

**What is a Data Protection Impact Assessment (DPIA)?**
A DPIA is a tool utilized to identify and analyze risks for individuals, which exist due to the use of a certain technology or system by an organization in their various roles (as citizens, customers, patients, etc.). On the basis of the outcome of the analysis, the appropriate measures to remedy the risks should be chosen and implemented. A Process for Data Protection Impact Assessment Under the European General Data Protection Regulation is required. 

**When is a DPIA required?**
A DPIA is only required when the processing is “likely to result in a high risk to the rights and freedoms of natural persons” (GDPR Article 35(1)), but processors must continuously assess the risks created by their processing activities to identify when a type of processing requires them to conduct a DPIA.   The questions in pay to procure process are to assist you in assessing whether a DPIA is required and should be re-evaluated periodically in light of the list of processing operations that the Supervising authority deems subject to the requirement of a DPIA.

The assessment of whether there is a need for a DPIA, as well as any required DPIA should be carried out *prior to* processing. 

**Why is a DPIA Conducted?**
The GDPR is concerned with "personal data", which it defines as “any information relating to an identified or identifiable natural person (data subject); an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person.” GDPR Article 4.  

The exception to this rule would be if an applicable exception applied. These exceptions are limited and unlikely to apply in most circumstances.  See, GDPR Article 35 (10).

You should limit the collection of the data, consistent with the HIPAA concept of "minimum necessary", to that which is genuinely required for the stated purpose. 

**How is DPIA Conducted?**
The System Owner is required to seek the advice of the Data Protection Officer when carrying out a DPIA.  Also, the System Owner must “seek the views of data subjects or their representatives” (Article 35(9)), “where appropriate”. 

At GitLab, the DPIA process is initiated by the System Owner during the [Procure to Pay Process](/handbook/finance/procure-to-pay/#procure-to-pay-process), during which the System Owner must complete a [Data Protection Impact Assessment issue template](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/new?issuable_template=Data%20Protection%20Impact%20Assessment). 

The GDPR provides that: 
- those views could be sought through a variety of means, depending on the context (e.g. a generic study related to the purpose and means of the processing operation, a question to the staff representatives, or usual surveys sent to the data System Owner’s future customers) ensuring that the System Owner has a lawful basis for processing any personal data involved in seeking such views. Although it should be noted that consent to processing is obviously not a way for seeking the views of the data subjects; 

- if the data System Owner’s final decision differs from the views of the data subjects, its reasons for going ahead or not should be documented; 

- the System Owner should also document its justification for not seeking the views of data subjects, if it decides that this is not appropriate, for example if doing so would compromise the confidentiality of companies’ business plans, or would be disproportionate or impracticable. 

Specifically, if there are remaining , unacceptable high risks such that,  as a result of the project:  "data subjects may encounter significant, or even irreversible, consequences, which they may not overcome (e.g.: an illegitimate access to data leading to a threat on the life of the data subjects, a layoff, a financial jeopardy) and/or when it seems obvious that the risk will occur (e.g.: by not being able to reduce the number of people accessing the data because of its sharing, use or distribution modes, or when a well-known vulnerability is not patched)". 

**Are there other resources?**
Guidelines on Data Protection Impact Assessment [DPIA] and Determining Whether Processing is "Likely to Result in a High Risk" for the Purposes of Regulation 2016/679. Article 29 Data Protection Working Party (Adopted April 4, 2017).  http://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=611236 . 

## CAN-SPAM

Since I live in Minnesota, it seems appropriate that I alert people to the nuances of canned spam; but as a lawyer, it is probably more important that I let you know about CAN-SPAM.  One of these items is very salty; the other is meat in a tin.  Controlling the Assault of Non-Solicited Pornography and Marketing Act (CAN-SPAM) came into effect in 2003 and impacts all mass electronic marketing communications (among other things). 

CAN-SPAM sets forth certain requirements that must be honored when sending out mass emails and other marketing materials.  They may seem simple but, much like canned spam, there is more than meets (meats?) the eye. 

*Keep the header honest.*  The reader needs to be able to identify who is sending them messages.  The email address of the sender should properly denote GitLab as the sender of the company material.  

*Keep the subject line honest.*  Don’t mislead the reader or otherwise use deceitful or inaccurate subject lines to compel someone to open the email. Be honest, impactful and short.  You can create urgency but, most importantly, create value.  In fact, the more authentic and clear the email, the less likely it is to be marked as spam or junk mail.

*Admit that it is an ad.*  This doesn’t mean that you have to go over the top and begin each Subject Line with “Hello, I am an advertisement.”  It just means that the sender has to know it is marketing material sent by GitLab. Put it in the subject line or put it in the body - just put it somewhere. 

*Location, location, location.*  Primary rule in real estate and an important rule in spam laws.  There needs to be a valid business address listed in the emails.  This helps ensure that if that Nigerian Prince were to send out an email on GitLab’s behalf - the recipient has a clear and legal point of reference to double check the authenticity of the sender.

*Opt-out options are not oppressive.*  When people receive mail that they don’t want, there must be an easy way to unsubscribe from the lists.  And “easy” means my grandmother should be able to figure it out as quickly as my 15-year old nephew.  In some jurisdictions, every email sent after an opt-out is selected is subject to a fine. 

*Opt-outs need to be quick.*  Ideally, an opt-out will occur automatically or within a business day or two.  If you take more than ten (10) business days to remove an email from a mailing list, things will get messy quickly.   

*Stay Diligent.*  If you use a third party to manage business emails, be aware that GitLab could still be on the hook for any wrongdoing.  Make sure that any mass communications are reviewed by someone knowledgeable in the spam laws.  

We care about our customers and their protection is our focus.  But, if creating transparent and open relationships with our customers isn't your concern, then be aware that failing to comply can cost a lot of money on a per email basis - up to $41,000 per violation.  That adds up quickly.  

The rules are simple and something each one of us would appreciate in our own inbox.    

## Cookies and Consent
 
I was happy to learn that if someone tries to give you a cookie, they have to get your consent.  I was a crazy diet and, despite my attempts to eat healthy, offers of Oreos kept surfacing.  Cookies can be detrimental if not kept under control.
 
Electronic cookies are the same.  
 
Privacy and Electronic Communications Regulations (PECR) sit alongside the Data Protection Act and the GDPR. They give people specific privacy rights in relation to electronic communications.  PECR sets specific rules around marketing, secure communication services and customer privacy (as regards traffic and location data, itemised billing, line identification, directory listings) and - you guessed it - cookies (which is broadly defined).
 
There are important factors to consider with cookies in general. 
If you use cookies you must:
•say what cookies will be sent;
•explain what the cookies will do; and
•obtain consent to store cookies on devices.
 
PECR expands the definition of cookies to include “similar technologies” like fingerprinting techniques. Therefore, unless an exemption applies, any use of device fingerprinting requires the provision of clear and comprehensive information as well as the consent of the user or subscriber.
 
Consent is not required if the cookie is used: “(a) for the sole purpose of carrying out the transmission of a communication over an electronic communications network; or  (b) where such storage or access is strictly necessary for the provision of an information society service requested by the subscriber or user.”
 
Please note that cookies for analytics purposes are not “strictly necessary”. 
 
PECR applies to any technology that stores or accesses information on the user’s device. This could include, for example, HTML5 local storage, Local Shared Objects and fingerprinting techniques.
 
Device fingerprinting is a technique that involves combining a set of information elements in order to uniquely identify a particular device. Examples of the information elements that device fingerprinting can single out, link, or infer include (but are not limited to):
•data derived from the configuration of a device;
•data exposed by the use of particular network protocols;
•CSS information;
•JavaScript objects;
•HTTP header information,
•clock information;
•TCP stack variation;
•installed fonts;
•installed plugins within the browser; and
•use of any APIs (internal and/or external).
 
It is also possible to combine these elements with other information, such as IP addresses or unique identifiers, etc.
 
PECR also applies to technologies like scripts, tracking pixels and plugins, wherever these are used.
 
If you provide cookies that fall in these categories, consent is required.  You never who is on a technological diet.  Ask before you give them a cookie.  

## Contract Details

I went to eat at a restaurant last week and the menu didn’t have any prices on it.  I was so excited to find a luxurious place that didn’t care about money!  I thought “Wow!  Great food and all in my price range!”  Man, was I surprised.  I was informed by my dinner guests that the lack of prices wasn’t a sign of gratuity.  

I then had to ping the waiter to get the going rate for chicken, a ballpark estimate on soup and the blue book value on beef.  After interrogating the poor waiter for a great deal of time, I finally settled on a nice side salad for my meal along with the restaurant’s finest l’eau de la tap. I pretended to thoroughly enjoy the evening even when the bill came.  Who needs new tires or brakes, anyways?  They’re overrated.  As I slid home with my doggie-bag of croutons rolling on my floorboard, I realized that the restaurant wasn’t the best fit for my needs.  

What does this mean to you?  Expectations.  The number one cause of corporate litigation isn’t bad sales people (as many lawyers might argue) or bad lawyers (as everyone may argue) – it is the misunderstanding of expectations. The contract is just the sword and shield wielded when the misunderstanding escalates.

So let’s talk about some fundamentals of contract terms and why those horrible lawyers wield them.  

Price - If a contract lacks a price (or a mechanism for objectively ascertaining price) then the contract isn’t valid.  If I agree to sell you my Defibrillator Toaster, the primary thing you want to know (aside from what is a Defibrillator Toaster) is how much does it cost?  If I get so crazy or convoluted with a pricing structure that a reasonable person cannot understand the cost, I could lose the validity of the contract.  No price, no deal.

Delivery – Just when is that toaster going to arrive?  If it is not in the contract, I can hold the toaster until I’m 100.  “Yeah.  I said I would sell it to you but not this year.”  Sounds crazy.  It is.  But it’s there because it happened frequently enough that courts just eventually wound up requiring some objective form of delivery to stop people from getting money right away for something they don’t plan to deliver until ten years later. Accounting rules also require clarity here, so have mercy on accountants.

LOUD NOISES – THESE ARE ALL THOSE CONTRACT TERMS IN THE AGREEMENT THAT ARE ALWAYS PUT IN CAPITAL LETTERS.  DO YOU KNOW WHY?  BECAUSE APPARENTLY LOTS OF PEOPLE SAID THAT UNLESS YOU ARE SCREAMING AT THEM, THEY DIDN’T REALIZE THE TERMS WERE THERE.  WELL, THE YELLING IS RESERVED FOR THE BIGGEST OF THE BIG. [SO IF YOU ARE EVER TOO TIRED TO READ AN ENTIRE CONTRACT, FOCUS ON THE SCREAMS.]  WE DON’T PAY FOR EVERY SINGLE THING THAT COULD POSSIBLY GO WRONG BECAUSE OUR PSYCHIC APP EXTENSION IS STILL IN DEVELOPMENT. WE AREN’T GOING TO FUND A CORPORATE RETREAT IN FIJI FOR SOME OTHER PARTY FOR SOMETHING WE NEVER SAW COMING OR WAS UNRELATED TO OUR PRODUCT. UNLESS YOU CLARIFY THIS, UP FRONT, SOMEONE WILL ALWAYS EXPECT YOU TO BANKROLL A PROBLEM.  WE HAVE TO BE CLEAR HERE. 

MORE LOUD NOISES – WE DON’T PROMISE OUR PRODUCTS WILL MAKE ANYONE LOOK YOUNGER, RUN FASTER OR CAUSE PATIENTS TO ACT LIKE THE SIX MILLION DOLLAR MAN OR BIONIC WOMAN.  OUR PRODUCTS DO WHAT THE DOCUMENTATION SAYS THEY WILL DO.  NOTHING MORE. 

UNDER WARRANTY, CUSTOMERS GENERALLY ASK FOR TWO THINGS: WARRANTY OF MERCHANTABILITY AND WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE. MERCHANTABILITY MEANS THAT WE ARE PINKY-SWEARING (AT THE RISK OF LOTS OF MONEY) THAT THE PRODUCT WE PROVIDE WILL PERFORM EXACTLY THE SAME WAY IT PERFORMS IN EVERY OTHER ENVIRONMENT.  THAT’S A SUPER TRICKY PROMISE TO MAKE SINCE NO TWO CUSTOMERS ARE IDENTICAL. 

FITNESS FOR A PARTICULAR PURPOSE MEANS THAT WE ARE CROSSING OUR HEART (AT THE RISK OF LOTS OF MONEY) THAT THE PRODUCT WILL DO EVERYTHING THAT THE CUSTOMER EXPECTS IT TO DO  - EVEN IF THEY HAVEN’T TOLD US THOSE EXPECTATIONS.  SO, UNTIL THE GITLAB PSYCHIC APP IS RELEASED, WE HAVE TO AVOID THIS ONE, TOO (OR OTHERWISE DO A MAGIC 8 BALL INSTALL).  

HERE’S ANOTHER JEDI-CONTRACT TRICK: IF YOU DON’T EXPRESSLY STATE THAT NO OTHER WARRANTIES APPLY THEN OTHER WARRANTIES WILL APPLY (EVEN THOSE NOT WRITTEN OUT).  CONFUSING, LAWYERS MAKE IT. 

Governing Law – We like to be certain of where any lawsuit may take place.  Think of it as wanting home court advantage. Even without the advantage, could you imagine having to travel to all fifty states or to dozens of different countries to deal with lawyers?  Scary, right? Not only is it expensive, it is completely unpredictable.  We might as well just try our odds at Vegas versus securing consistent verdicts in California, Louisiana and Delaware [For OUS, it would be like trying to get consistent verdicts in France, Russia, Germany and Zimbabwe.]  It’s not a reflection on any particular system other than that they are all very different.  You may win big but you may also lose big for the exact same issues.

Zombie Clause – this is to make sure that some things still haunt you long after the contract is dead.  We don’t want our customers buying our stuff, promising not to reverse engineer it, cancel the contract and then yell “Psych! It’s our IP now!”  Canceling a contract doesn’t cancel the obligation to protect our IP or maintain confidentiality.  These things survive after the death of the contract – like Jason or Freddy or most other horror movie characters.  

STOP YELLING AT ME – I FORGOT ABOUT INDEMNIFICATION.  THIS IS WHERE WE PROMISE TO PAY FOR LAWSUITS THAT A CUSTOMER MAY HAVE.  IF WE DON’T CLARIFY HERE, WE COULD BE PAYING FOR THINGS UNRELATED TO OUR PRODUCTS. YEAH.  IT’S A REAL BIG DEAL; EXPENSIVE, TOO.  IN FACT, THIS IS ONE OF THE MOST EXPENSIVE PROVISIONS IN A CONTRACT. 

Intellectual Property – By Intellectual Property, I mean patents, copyrights, trademarks, trade secrets, pictures, videos, sounds (ummm, not sure what kind of sounds we’ll make…but they're ours!!)  I had to deal with a great many patent lawyers to make sure that we have a competitive advantage.  Dealing with all these lawyers has aged me considerably.  Did you know that I am only 21?  So, we need to protect our IP out of respect for our work, oh, and because it gives market differentiation. Any attempt to sell or give away IP ownership has to have to permission of the highest levels of management.  (By highest, I mean by rank, not altitude.) 

Confidentiality – I can’t talk about this.  I am pretty sure most all of you know why we have this.  If not, send me a coded message and we can meet in an undisclosed Caribbean island, at your expense, to make sure you understand this need. 

Subtext – Many people are unaware of the legal baggage that accompanies many phrases.  For instance:

When you see “Time is of the essence”, you may think.  I guess they are in an eloquent hurry.  Nope.  When you see that phrase, it means a late delivery is a material breach of contract and, DEPENDING ON HOW MUCH YOU SCREAMED EARLIER, you could be on the hook for damages.  So, Herbal Essence, okay; Time is of the essence, ugh.

When you see “best efforts”, you may think that you should try real hard.  Nope.  This phrase means that you will go as far as bankrupting the company before you fail on that effort.  It’s not logical but it keeps lawyers employed; so we like it.  In fact, it’s like a legal booby-trap.  Those without lawyers may get caught in the snare.

When you see “I’m fine. Forget about it.”  You had best be buying flowers and chocolate because you’re in trouble.

*While these are just some high level things to consider when reading a contract, some restrictions may apply.  Not available in all countries. Quantum materiae materietur marmota monax si marmota monax materiam possit materiari? Si Hoc Legere Scis Nimium Eruditionis Habes

## Trademarks
 
A trademark is a word, symbol, device, or any combination used to identify and distinguish one's goods or services from others.  It is a form of Intellectual Property and it has value; so much value that using someone else’s trademarks improperly or failing to protect your own can cost companies large amounts of money.   

I bring this up because I am thinking about selling soda out of my garage to offset my wages.  For fifty cents (the money, not the rapper), you can get my Yummy Upper Cola.  Now, the adventurers out there may feel compelled to imbibe my crazy new concoction but I recognize that the majority will most likely default to the more well-known and established brands.  The certainty of the name ensures certainty of the product; and therefore, the certainty of the sale.  

That said, I am still confident that my soda with its secret sauce handed down from a guy I met at the airport will astound you.  I just need to finalize the name and build the brand. 

There are important things to consider when settling on a name and ensuring I get the most value from my mark as possible.

**Strength:** The ideal trademarks are fanciful or arbitrary. Fanciful is just a nice way of saying “made up”.  This category includes marks like Kodak, Xerox, iPod, Lego, etc.; you know, those words that had absolutely no meaning before they were trademarked.   Arbitrary just means that the name and the product are not generally found together or otherwise related.  For instance, computers and apples generally don’t go together (unless you happen to be a tech-savvy Adam and Eve); therefore, Apple has a very strong mark.  When trying to decide is a mark is arbitrary, consider this: if the company name and the product were SAT or ACT questions, they would be tough (if not impossible) to answer. To demonstrate:  Shell is to gasoline as Dolphins are to _____; Hertz is to cars as Digestion is to _____________; Amazon is to Shopping as Urology is to ____________. The mere arbitrariness of the marks makes them difficult to penetrate (and to word associate).

**Use:** Unlike patents, in order to protect a trademark – you must use it.  You can’t register it and then let it sit on the shelf until you decide what to do with it.  It must be used AND it must be used in the manner with which you said you would use it. I can’t name my soft drink Yummy Upper Cola and then label the bottle “YUC”.  I need to use the words I trademark and I need to trademark the words I use.  When actually using the mark, the best protection for the mark is to use it as an adjective.  Once you start using the mark as a verb or a noun – you make it generic.  You can’t enforce generic.  Even fanciful words can become generic if used improperly as a verb or noun.  Think about it, how often have you used a Q-Tip to clean your ears while Rollerblading?  The resulting fall required you to apply a Band-aid, wipe your tears with a Kleenex while Tasering those who laughed at your misfortune. If you doubt that these words are trademarked, Google them. Misusing even a great trademark can make it so common that it can get difficult and expensive to protect. 

**Market:** Don’t think that just because you got a trademark that no one else in the world, country, state or neighborhood can use that same word.  There is Delta Airline, Delta Dental and Delta faucets.  The key distinction here is likelihood of confusion.  If you see “Delta” on a plane – you know that it is the airline company.  If you see “Delta” on a sink faucet, is there a real likelihood that you would expect frequent flier miles from it? Probably not. So two very different companies can use the same name because most people are smart enough to realize that these are very different companies.  

While you can have the same word for different markets, you can’t have different words for similar markets if those different words are similar to a trademarked word (back to that likelihood of confusion standard).  If I decided to take my soda sales global – I would not be able to trademark Boca-Bola, 6Up, 7Down or Spite.  While none of these names are currently trademarked - and they are quite fanciful or arbitrary – they present a risk of confusion.  The names are close - and close counts in horseshoes, hand grenades and trademarks. 

I should note that trademarks aren’t just words.  Owens Corning trademarked the color pink for insulation. No other company can make insulation that color.  If you ever see insulation that is pink – you know who made it. But if you see the singer Pink, there is no of confusion, she doesn’t sell insulation. John Deere trademarked a specific color of green for farm equipment.  McDonald’s has the Golden Arches.  Sounds can be trademarked.  NBC trademarked the chimes that you hear during intermissions, Intel has a trademarked sound. Shapes can be trademarked! One of the most famous non-word trademarks is the shape of a Coke bottle.  Once you start getting into how something looks – it is called trade dress. Trade dress is just a fancy term for trademarked product presentation.  If my sodas were sold in curvy bottles, I would be poking the seltzer giant.  I would be infringing.

**Registration:** Another thing to consider is if I should register the mark.  In some common law countries (such as US, UK, South Africa, Australia, et al), those who use the mark first get protection. (These trademarks are denoted by a  “TM”.) Admittedly, it can get difficult to determine when that first use occurred; so registration in common law countries helps to clear that date issue up.  In any non-common law country, the first to file owns the mark. To set a date certain and own the mark, marks get registered.  If a mark is registered, it gets an “®”.  [That is a R with a circle around it for those with struggling eyes.]  The catch here is that when you put that R on your mark, you are in essence screaming “Mine, mine! I own it here!”  The problem with this attestation is that if I registered my soft drink in Canada but I send the bottle marked Yummy Upper Cola® to Germany, the Germans could easily yell at me “Das ist nicht richtig!” They would be right, I wouldn’t be right. The mark isn’t registered in Germany, so the R suggests a sort of untruth.  To combat this confusion, whenever you use an R for a mark, you should also state where the mark is registered.  ”Yummy Upper Cola is registered in Canada and may have protection in other countries.” That way, the Germans know that I am not trying to con them. I can either use the R for my sales in registered countries and no mark everywhere else or just use the caveat.  

**Cultural Significance:**  Last but not least, cultural sensitivity is critical.  Any attempt to market globally and/or translate trademarks must be met with heightened attention.  As my “YUC” moniker is unlikely to draw a positive response, many other companies have attempted to trademark words and failed miserably. 

Coors put its slogan, "Turn it loose," into Spanish, where it was read as "Suffer from diarrhea". 

Clairol introduced the "Mist Stick", a curling iron, into Germany only to find out that "mist" is slang for manure. Not too many people found a use for the "manure stick". 

In Chinese, the Kentucky Fried Chicken slogan "finger-lickin' good" came out as "eat your fingers off". 

An American T-shirt maker in Miami printed shirts for the Spanish market which promoted the Pope's visit. Instead of "I saw the Pope" (el Papa), the shirts read "I saw the potato" (la papa). 

In Italy, a campaign for Schweppes Tonic Water translated the name into "Schweppes Toilet Water". 

Pepsi's "Come alive with the Pepsi Generation" translated into "Pepsi brings your ancestors back from the grave", in Chinese. 

When Parker Pen marketed a ball-point pen in Mexico, its ads were supposed to have read, "it won't leak in your pocket and embarrass you". Instead, the company thought that the word "embarazar" (to impregnate) meant to embarrass, so the ad read: "It won't leak in your pocket and make you pregnant". 

The Coca-Cola name in China was first read as "Ke-kou-ke- la", meaning "Bite the wax tadpole" or "female horse stuffed with wax", depending on the dialect. Coke then researched 40,000 characters to find a phonetic equivalent "ko-kou-ko- le", translating into "happiness in the mouth".

Like with any conversation, it is critical to be aware of how your words are received in different cultures. 
Enjoy the education! TM

## Secret Reseller Man

“Secret Reseller Man, they've given him a number and taken away his name.”  Hmm.  Something doesn’t sound quite right there.  Oh, yes!  It’s Secret Agent Man!  Clearly Agents and Resellers are NOT synonymous. It totally screws up song lyrics.  But what are the distinctions?  I am so glad you asked because I currently need to sell my stash of glow in the dark toilet paper and this is the perfect opportunity.  I am selling it for $5 a roll and I have a hundred rolls. 

So, you know about my inventory.  You know people that would love this toilet paper.  You have contacts. You tell me that you want to be my **reseller**.  Resellers buy from the Manufacturer and, you guessed it, resell the product.  As my reseller, I would sell you my toilet paper at $4 roll.  I’m giving you a reduction in price because you are saving me time and money in chasing down buyers.  Once you buy my toilet paper, it’s yours.  I can have no control over what you decide to do with it  - except, if you do something illegal with it.  If you do something illegal with it,  I just might have full liability.  This is why the most critical part of our arrangement is my demand that you not do anything illegal with the toilet paper I sell you. 

Aside from requiring you to not break any laws, once you buy my toilet paper, I can have no more control with what you do with it.  I can’t tell you how much to sell it to others, I can’t tell you to combine it with other products, I can’t tell you where to sell (or not to sell).  For all intents and purposes, it is yours.  If you want to use the toilet paper as decoration at a party - Mazel Tov.  If you want to wear it as headwear - Oo La La.  I don’t want to know who your customers are or what they paid.  They are your problem!  Your agreements are your to manage.  The customers will transact directly with you. And, if the customers get mad, it is you they will sue.  If I were to try to start dictating what a reseller can and can’t do, I could trigger antitrust laws. Antitrust laws would get us both in hot water.  

Think of all the things you have bought in your life.  Can you imagine how horrible and controlling it would be if anything you tried to resell was subject to the manufacturer’s control?  Garage sales would be a legal nightmare.  “Yes, Pierre, I would love to sell you this used monitor of mine but the manufacturer insists that I charge you $300.” Wills and trusts would be bureaucratic due to the minimum exchange amounts and new terms..it would be silly.

An important fact to note is that because I am selling to you, you are major customer of mine.  Why does that title matter?  Because I cannot PAY customers to buy from me; no one can.  This is called a kickback.  If you pay a reseller, you could run afoul of anti-bribery laws. 

Resellers make money by buying low and selling high.  (You can choose to buy high and sell low and I can’t stop you.  Not the greatest idea but, hey, it’s your problem once I’ve sold to you.)  Resellers control their own sales, revenues, losses and markets. A good reseller makes a profit running their business. 

Now, you could decide to be a **representative or agent** for me.  This is different than a reseller.  As my agent, you would have all your friends call me to buy my toilet paper at $5 a roll directly from me.  For every roll that I sell, I will pay you $1 commission.  The difference here is that the agreement is between me and the customer.  You wear my hat and hold yourself out as an extension of me.  I pay you a percentage of the sales; but those sales are at full price.  

If you were my agent and did something pretty bad, the customers would sue me directly.  Sure, I would probably make sure that I had a strong indemnification clause in my agreement with you to make sure you cover me but I am 100% on the hook with the customer issues.  

What about **distributors**?  Can you be, won’t you be - my distributor?  The nuance here is that a distributor is a glorified reseller.  Where a reseller sells to the end user, distributors sell to resellers.  

**Why would anyone care about these nuances?  Because the relationship will govern the contract terms.  The relationship also governs which laws apply.**

If you are reselling or distributing, it is critical that I give you the right to resell.  It is critical that I demand you comply with anti-corruption, anti-bribery, anti- slavery/human trafficking and export laws because I have no insight into what you are doing but I could face civil and criminal if you do something illegal.  I want to make sure that you know that I am not paying you, I will not control you and my toilet paper is $4 a roll for you.  I also need to clarify certain responsibilities.  Since the reseller has the relationship with the customer, any rights that go with the product also need transfer rights.  For instance, while the customer may only know the reseller, the right to customer support from me may need to be flowed down to the end user; or do I want the reseller to be first line support?  So many questions.    

The distribution agreement has a bit more complexity because there is one more party between me and the end wiper.  The rights need to be transferred more than once. 

If you are my agent, I need to set very clear expectations.  If you are holding yourself out as an extension of me, I have standards that must be met.   You need to understand and agree to those standards.  I can control where you sell, to whom you sell and for how much you offer the product.  Since my contract is directly with the customer, you don’t need any specific rights as it relates to the product.

Ultimately, the key to remember is that a reseller is a customer who is going to resell the product.  They buy at a lower (transfer) price and run free (within the bounds of the law). Resellers negotiate directly with the customer and they bear most of the risk.  

Distributors resell to resellers.  The distributor and reseller (in their own agreements) allocate risk between and among themselves. The ultimate sale to the end user is under a reseller agreement.  

The manufacturer is liable for the bad acts of its resellers and distributors, its own actions and product liability.  The distributor and reseller are on the hook for most everything else.

Representatives or agents facilitate sales between the manufacturer and end user and they are paid a commission.  Agents don’t absorb much risk as this acts like a direct sale.  The manufacturer is on the hook for everything.  This sounds scary but since a manufacturer can exercise control over an agent, it is a risk that is easier to mitigate. 

In the time it has taken me to write this, I have already blown through most of my inventory.  Hopefully, I can still sign you up as a reseller for my remaining rolls. 

For more information, see the [Partner Code of Ethics](/handbook/people-group/code-of-conduct/#partner-code-of-ethics)

 
## Export

I grew up in a time where the only social media we had were comments written in yearbooks and on bleachers.  Thank goodness those embarrassing pictures of me never made it to the eternal technological world.  You know those pictures - the ones taken right after you made a profound statement such as “Here, hold my beer…” or “That’s nothing, watch this…”  Inevitably, what followed was always incriminating.  

Don’t get me wrong, those pictures still exist.  They remain hidden in a photobox in the depths of my personal archives.  There are a few, select people with whom I share these most embarrassing moments.  That said, there are some people who I will never share those photos; specifically, my ex, those who live in my ex’s house and Sid.

You could say that I have imposed export restrictions on the pictures for those people and areas.  My photos can be shared with anyone EXCEPT those I have specifically embargoed or denied. 

Governments throughout the world pose similar restrictions (not on my pictures, on their technology).  Various countries state that if the technology is of their origin, it falls under the laws of their country.)  It isn’t just a United States thing, most countries have restrictions.  Even Canada has rules about their technology and Canada has to be one of the nicest countries in the world.  Who doesn’t like Canada? 

Well, GitLab US technology is subject to US law.  Much like I say that you can’t send my pictures to anyone at my ex’s house, US law states you can’t send their technology to Iran, Sudan, Syria, Cuba, North Korea or the Crimean Region of the Ukraine. Much like I say that you can’t show my pictures to my ex or to Sid, US law has a list of entities to whom they prohibit passing the technology.  

A prohibition is a prohibition.  You can’t find loopholes.  For instance, if I say you can’t send those pictures to my ex, you can’t try to get all creative by giving him a tour of my house and just showing him the pictures while he is here.  You can’t take pictures of my pictures and forward those.  You can’t have a sketch artist recreate renderings of the photos.  Same with export.  If you can’t send the technology to a country or a person, you can’t let them see the guts of the technology through other mechanisms.  There simply aren’t loopholes in export law.  The only way you can get around the restrictions is to get permission from the government directly.  This permission is provided in the form of a license. 

Violating my picture restrictions will definitely get you in trouble.  There will be stern lectures, cold stares followed by serious silence.  Violating export laws can result in civil and criminal penalties for the officers of the company and (in case you really don’t care what happens to the officers) the individuals involved.  

Let’s keep things on the up and up - don’t violate either. 

For more information, check out:
[Export Policy](/handbook/people-group/code-of-conduct/#trade-compliance-exportimport-control)
[Export Procedure](/handbook/business-ops/order-processing/#trade-compliance-export--import-and-visual-compliance-tool-in-salesforce)




Questions about Compliance?  Contact cciresi@gitlab.com



