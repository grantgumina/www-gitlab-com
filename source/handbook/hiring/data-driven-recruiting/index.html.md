---
layout: handbook-page-toc
title: "Data Driven Recruiting"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Data Driven Recruiting

[Dave Gilbert](https://about.gitlab.com/company/team/#davegilbert), [Sid Sijbrandij](https://about.gitlab.com/company/team/#sytses) and Brendan Browne discuss how to generate a Data Driven Recruiting Model.

<figure class="video_container">
  <iframe src="https://www.youtube.com/watch?v=8qNxeEJimpU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Data Driven Recruiting at GitLab

TODO: VP of Recruiting to add iterative steps to works towards following a Data Drive Recruiting model at GitLab.
