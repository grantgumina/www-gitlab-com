---
layout: handbook-page-toc
title: Certifications
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the certifications page! Here you will find links to our current certifications.

## Current Certifications 

* [GitLab Values](/handbook/values/#gitlab-values-certification)
* [GitLab Communication](/handbook/communication/#communication-certification)
* [Being an Ally](/company/culture/inclusion/ally-training/#ally-certification)

## Upcoming

In FY21-Q1 we plan to roll out the additional certification(s) listed below. 

* Inclusion
