---
layout: handbook-page-toc
title: "Drift"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Using Drift

Drift is the online chat bot and tool SDR's use to engage with site visitors. This page will be updated as it is rolled out across the teams. For a quick reference guide to start please click [here](https://docs.google.com/document/d/1RLnhUuc-_G_XDcmnDjOD-KbF-mQNIB4fYR281VN_8dY/edit?ts=5e1f4f04).


## Rules of engagement  
### **SDR**

-   SDR to turn chat availability off when done working / no longer want to receive chats
-   SLA - 30 seconds
-   Expectations for SDR managers is that they will coach reps to engage Drift leads quickly 
-   What do you do when something goes wrong?
    -   Wrong lead
        -   If lead is incorrectly routed SDR will reroute and notify the correct SDR
    -   Escalation
        -   Managers looped in
        -   Close conversation if situation isn’t resolved
        -   Mark conversation within Drift (Ops to determine the best way to do this, sales enablement trainings will occur)


## Drift training

Enablement videos will be linked here once they are ready.


## Drift requests  

Please open an issue in the MktgOps project. State the reason for the issue and leave any detailed information in regards to the issue or request being made.

Labels to use:  
- `MktgOps`: The starting point for any label that involves MktgOps. The MktgOps team subscribes to this label and will see the request being made.
- `Drift`: Issue is directly related to the Drift chat tool. Specific admins for this tool are subscribed to this label.


## Drift routing

Expectation is that Drift routing will mirror [LeanData](https://about.gitlab.com/handbook/marketing/marketing-operations/leandata/) as closely as possible. If there is an exception it will be listed here as more complex routing is rolled out.

Leads will only be routed to the aligned SDR:
-   Meaning if someone uses chat during off hours, they won’t be sent to a SDR in another region. They’ll be pushed to schedule a meeting.

## Drift playbooks


| Type | Name | Pages|
| :--- | :--- | :--- |
| Online | NEW Pricing Page Bot - ONLINE (7/19/19)|https://about.gitlab.com/pricing/
| Online | Have More Qualified Conversations (14 March 2018) |https://about.gitlab.com/sales/ https://about.gitlab.com/free-trial/ https://about.gitlab.com/features/ https://about.gitlab.com/comparison/
| Offline | OFFLINE: High-Intent Bot |https://about.gitlab.com/pricing/
