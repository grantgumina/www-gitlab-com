---
layout: markdown_page
title: "Category Direction - Continuous Delivery"
---
 
- TOC
{:toc}
 
## Continuous Delivery
 
Many teams are reporting that development velocity is stuck; they've
reached a plateau in moving to more and more releases per month, and now
need help on how to improve. According to analyst research, 40% of software development
team's top priorities relate to speed/automation, so our overriding vision
for Continuous Delivery is to help these teams renew their ability to
accelerate delivery.
 
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Delivery)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/ci/)
 
### Continuous Delivery vs. Deployment
 
We follow the well-known definitions from Martin Fowler on the
difference between continuous delivery and continuous deployment:
 
- **Continuous Delivery** is a software development discipline
 where you build software in such a way that the software can
 be released to production at any time. You achieve continuous
 delivery by continuously integrating the software done by the
 development team, building executables, and running automated tests
 on those executables to detect problems. Furthermore you push the
 executables into increasingly production-like environments to
 ensure the software will work in production.
- **Continuous Deployment** means that every change goes through the
 pipeline and automatically gets put into production, resulting in many
 production deployments every day. Continuous Delivery just means that
 you are able to do frequent deployments but may choose not to do it,
 usually due to businesses preferring a slower rate of deployment. In
 order to do Continuous Deployment you must be doing Continuous Delivery.
 
_Source: [https://martinfowler.com/bliki/ContinuousDelivery.html](https://martinfowler.com/bliki/ContinuousDelivery.html)_
 
### Incremental Rollout
 
One of the key sub-areas within Continuous Delivery that we're tracking
is, incremental rollout. This is important because it enables unprecedented
control over how you deliver your software in a safe way.  For these
items we're also tagging the label "incremental rollout", and you can
see an issue list of these items [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AIncremental%20Rollout).
Incremental rollout also serves as a key pillar for our [Progressive Delivery](/direction/cicd/#theme-progressive-delivery)
strategy.
 
### Infrastructure Provisioning
 
Infrastructure Provisioning and Infrastructure as Code, using solutions like Terraform or other provider-specific methods, is an interesting topic that relates to deployments but is not part of the Continuous Delivery category here at GitLab. For details on solutions GitLab provides in this space, take a look at the [category page](/direction/configure/infrastructure_as_code/) for our Infrastructure as Code team.
 
In short, the Continuous Delivery category will help you deploy the code in your project to your infrastructure; our Infrastructure as Code category will help you create and modify your infrastructure as part of your deployments.
 
### Kubernetes Deployment with Auto DevOps
 
For deployment to Kubernetes clusters, GitLab has a focused category called Auto DevOps which is oriented around providing solutions for deploying to k8s. Check out their [category page](/direction/configure/auto_devops/) for details on what they have planned.
 
## What's Next & Why
 
GitLab CI/CD was built from the ground up with speed and scalability in mind, and we are always working hard to enable [Speedy, Reliable Pipelines](https://about.gitlab.com/direction/cicd/#speedy-reliable-pipelines). 
With that in mind, we are working on [gitlab#25276](https://gitlab.com/gitlab-org/gitlab/issues/25276) which allows 
only forward incremental deployments. This will solve the problem of concurrent deploys and verify that our deployment is latest successful one,
Another important theme that we are working on is [Do Powerful Things Easily](https://about.gitlab.com/direction/cicd/#do-powerful-things-easily), which includes 
[Natively supporting hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804). 
To start we are focusing on creating base images that will make it easier to use GitLab for deployment to 
any one of the major cloud providers, starting with [Streamline AWS Deployments](https://gitlab.com/groups/gitlab-org/-/epics/2351), even when not using Kubernetes. 
We are now working on [gitlab#26777](https://gitlab.com/gitlab-org/gitlab/issues/26777) which will introduce predefined AWS variables to the environment variables
which will make the integration easier and simpler. 
 
 
## Maturity Plan
 
This category is currently at the "Complete" maturity level, and
our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:
 
- [Run pipeline on merge result](https://gitlab.com/gitlab-org/gitlab-ee/issues/7380) (Complete)
- [Parallel execution strategy for Merge Trains](https://gitlab.com/gitlab-org/gitlab-ee/issues/11222) (Complete)
- [Allow fork pipelines to run in parent project](https://gitlab.com/gitlab-org/gitlab/issues/11934)
- [Limit pipeline concurrency using named semaphores](https://gitlab.com/gitlab-org/gitlab/issues/15536)
- [More out of the box incremental rollout options](https://gitlab.com/gitlab-org/gitlab-ee/issues/1387)
- [Streamline AWS Deployments](https://gitlab.com/groups/gitlab-org/-/epics/2351)
- [Allow only forward deployments](https://gitlab.com/gitlab-org/gitlab/issues/25276)
- [Post-deployment monitoring MVC](https://gitlab.com/gitlab-org/gitlab-ee/issues/8295)
- [Better guidance for more CD approaches](https://gitlab.com/gitlab-org/gitlab-ce/issues/52875)
- [Actionable CI/CD metrics](https://gitlab.com/gitlab-org/gitlab-ee/issues/7838)
 
 
## Competitive Landscape
 
Because CI/CD are closely related, the [competitive analysis for Continuous Integration](/direction/verify/continuous_integration#competitive-landscape)
is also relevant here. For how CD compares to other products in the market,
especially as it relates to pipelines themselves, also take a look there.
 
As far as CD specifically, has always been strong in providing actionable metrics on
development, and as they move forward to integrate GitHub and Azure
DevOps they will be able to provide a wealth of new metrics to help
teams using their solution. We can stay in front of this by improving
our own actionable delivery metrics in the product via [gitlab-ee#7838](https://gitlab.com/gitlab-org/gitlab-ee/issues/7838).
 
Spinnaker and Harness are also two modern, cloud-native CD platforms
that provide excellent solutions for delivering to cloud environments
using native approaches like Kubernetes. In order to remain competitive,
we must provide more advanced deployment operator solutions via
[gitlab#1387](https://gitlab.com/gitlab-org/gitlab/issues/1387).
Additionally, these products manage the deployment all the way through
to monitoring, which we will introduce via [gitlab#8295](https://gitlab.com/gitlab-org/gitlab/issues/8295).
If you are currently using Spinnaker as your CD solution, we would love to learn more. Please fill out our survey or register for
a customer interview at [gitlab#197709](https://gitlab.com/gitlab-org/gitlab/issues/197709).
 
We are conducting research on [Harness](https://gitlab.com/gitlab-org/gitlab-foss/issues/40722) and invite you to chime in on the issues and provide your insights and feedback as well.
 
### Spinnaker
 
Spinnaker is an open-source, multi-cloud continuous delivery platform that helps you release software changes.
It combines a powerful and flexible pipeline management system with integrations to the major cloud providers. 
Spinnaker is a great CD tool, but it does not support other stages of the DevOps lifecycle (such as Continuous Integration), 
while GitLab offers a single tool for your Development needs. 
Some of Spinnaker's advantage points are: 
 
* Provides easy application deployment across cloud providers. We have also recognised this as one of our top vision items and are working to [natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804).
* Supports several deployment strategies which we are working on expanding in [Advanced Deploys](https://gitlab.com/groups/gitlab-org/-/epics/2213).
* Allows configuring an environment in standby for easy rollback, which is closely tied to [gitlab#35409](https://gitlab.com/gitlab-org/gitlab/issues/35409).
* Provides a user interface that allows you to view your infrastructure and to see exactly where your code is residing.  
 
We also respect our customers choice to use Spinnaker's CD solution together with GitLab and are working on making
that integration easier with [gitlab#120085](https://gitlab.com/gitlab-org/gitlab/issues/120085).
 
## Analyst Landscape
 
In our conversations with industry analysts, there are a number of key trends
we're seeing happening in the CD space:
 
### Cloud Adoption
 
Cloud adoption of CI/CD is growing, with Docker adoption leading the
way and serverless likely next. People need guidance solving CD
because they are stepping out into the dark without mature solutions
already available to them; for example, AWS' current solution for
serverless (functions) CI is just "edit them live." Customers
complained that their products were starting to feel legacy, but
where they go next is unclear.
 
We invite you to follow our plans to [natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804) and to offer feedback or ask questions.
 
### Customer Experience
 
Customer experience is becoming a key metric. Users are looking for
the ability to not just  measure platform stability and other
performance KPIs post-deployment but also want to set targets for
customer behavior, experience, and financial impact, as part of [gitlab#37139](https://gitlab.com/gitlab-org/gitlab/issues/37139). Tracking and
measuring this indicators after deployment solves an important
pain point. In a similar fashion, creating views which are managing
products not projects or repos will provide users with a more
relevant set of data.
 
### Other Topics
In addition to supporting the trends above, there are some key areas
where we can focus that will improve our solution in areas that
analysts are hearing customer demand:
 
- **Modeling (and autodiscovery/templatizing) of environments and deployments**
 as-code, and in a way that they can automatically interact with each
 other to generate deployment plans without requiring scripting. Views
 that make these complex relationships between environments and
 deployments clear.
- [gitlab#8295](https://gitlab.com/gitlab-org/gitlab/issues/8295): **Deployment status reporting, monitoring behavior, and error handling**,
 including [automated rollbacks](https://gitlab.com/gitlab-org/gitlab/issues/35404), [incremental rollouts](https://gitlab.com/gitlab-org/gitlab/issues/27264), and other intelligent
 built-in and customizable strategies and behaviors. This can include
 predictive analytics/ML and reporting about successes, failures, and pipeline health,
 both retrospective and runtime. Customizable and tailored for the
 user/role engaging with the data.
- **Configuration drift detection** between what's in our internal model of
 what's deployed, vs. what's actually in the environments with alerting and
 potential automatic remediation.
- [gitlab#11222](https://gitlab.com/gitlab-org/gitlab/issues/8295): Take control of your software delivery by **assembling associated changes into merge trains**
 which can flow through your environments and on to production as a coherent
 group. 
 
## Top Customer Success/Sales Issue(s)
 
The ability to monitor deployments and automatically halt/rollback deployment in case of exceeding a specific error rate is frequently mentioned by CS
and in the sales cycle as a feature teams are looking for. This will be
implemented via [gitlab#8295](https://gitlab.com/gitlab-org/gitlab/issues/8295).
 
## Top Customer Issue(s)
 
Our most popular customer request is [gitlab#15536](https://gitlab.com/gitlab-org/gitlab/issues/15536),
When there are consecutive deploys to the same environment, it may cause unwanted problems.
We are working on an ability to allow users to limit concurrency to prevent simultaneous deploys from different projects to the same environment.
 
## Top Internal Customer Issue(s)
 
Our most popular internal customer issue is adding a way to notify the user about the maximum commits behind the source branch
[gitlab#26691](https://gitlab.com/gitlab-org/gitlab/issues/26691), this will reduce the risk of breaking master and let the user know that he/she should rebase before merging.
 
### Delivery Team
 
In an effort to dogfood our own features, we are actively working on using merge trains internally on gitlab-org [gitlab#195](https://gitlab.com/gitlab-org/quality/team-tasks/issues/195)
A design document is coming from the delivery team for the actual process. we can use
this to see how this would be modeled in GitLab; see also the
[CI/CD Blueprint](/handbook/engineering/infrastructure/library/ci-cd/)
 
- [gitlab#26691](https://gitlab.com/gitlab-org/gitlab/issues/26691) sets a maximum commits behind to merge
 
Our delivery team is also working on [gitlab#35741](https://gitlab.com/gitlab-org/gitlab/issues/35741) which displays the deployment information on the merge request, including when the first deployment took place, and also will tell you for a specific deployment which merge requests were merged. 
 
### SRE Team
 
From an SRE standpoint, see:
 
- [gl-infra#52](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/52) takes over management of the CI/CD infrastructure
- [gl-infra#51](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/51) takes over on-call for CI/CD infrastructure
 
 
## Top Vision Item(s)
 
 
Our top vision item is to [Natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804), we want to help make it easier and quicker to get started and deploy to any one of the big cloud providers using GitLab's CI/CD.
 
 

