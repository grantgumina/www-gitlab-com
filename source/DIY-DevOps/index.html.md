---
layout: markdown_page
title: DIY DevOps
---

You can build your own "Do it yourself" DevOps solution for your team, not unlike building a car from parts at the auto store, or building your own computer from scratch.  There are many reasons why there is value in working with a DevOps Platform, rather than the DIY approach.


|								|  DIY DevOps		|	Open Source DevOps Platform  |
| --- | ---| --- |
|  1. Developer Experience		|  Fragmented across multiple tools | Coherent and consistent throughout GUI and CLI command semantics |
|  2. DevOps tool innovation 		| Limited to your internal team  |  Community fueled innovation with thousands of contributors- Ability to influence roadmap and direct stakeholders as well as visibility through out maturity process. |
|  3. Reporting/Visibility  | Silos you have to integrate to access data  |  Single Data store makes it easy to   |
|  4. Developer On-boarding  | Must learn multiple tools and interfaces. | Industry standard platform - fast on-boarding. Up to date docs and a constellation of community-driven resources: meetups, courses, tutorials, etc. |
|  5. Access Controls & Permissions |  More risk as you must configure & manage access on multiple tools  |  Manage access in one place and reduce risk |
|  6. Tool Integrations  |  Custom integrations requiring ongoing maintenance & management. |  No need for integrations - Single application  |
|  7. Compliance/Auditing |  Challenging because data and evidence isolated in different tools |  Compliance and audits streamlined with data in one place |
|  8. Focus | Dev focused. | Dev Sec Ops focus  |
|  9. Efficiency and Bottlenecks. | Work can get lost between tools. |  Visibility with everyone on the same page  |
|  10. Credentials. | Passed from tool to tool to tool. |  Access to one tool |
|  11. New Versions  | May break existing siloed tools. |  Just works. |
|  12. Project Start up. |  Requires updates and configuration in multiple tools. |  Just create project in one place and start coding  |
|  13. Developer Efficiency  |  Time wasted switching context between tools  |  Developers are faster because they can focus on the work, not the tools.  |
|  14. Organizational Silos (Dev, Sec, Ops, etc)  |  separate tools reinforce the silos and reduce collaboration  |  Teams collaborate and work together in a single platform - bridging organizational silos. |
|  15. Security  |  a separate silo, often isolated. | Security scanning built into the lifecycle. |
|  16. Operational Cost  | Must manage and maintain the tools, tool integrations, licenses, upgrades  |  Lower cost, with just one platform to manage |
